<?
	
	// load in all of the classes
	require 'classes/Cart.php';
	require 'classes/Discount.php';
	require 'classes/IceCream.php';
	require 'classes/Milk.php';
	require 'classes/Soda.php';
	require 'classes/Item.php';
	require 'classes/IceCreamCone.php';
	require 'classes/MilkShake.php';
	require 'classes/Float.php';
	require 'controller.php';
	
	session_start();
	
	// get the action
	$action = $_GET['url']?$_GET['url']:'index';
	
	// map actions to controller method
	$controller = new Controller();
	
	if( method_exists($controller, $action) ) {
		
		$controller->$action();
		
	} else {
		
		$action = '404';
		
	}
	
	// bring in views
	include 'views/header.php';
	
	include ( 'views/' . $action . '.php' );
	
	include 'views/footer.php';
		
?>
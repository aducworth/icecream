<div class="container">
	<div class="row">
		
		<? if( $controller->errors ): ?>
					
			<div class="alert alert-danger" role="alert"><?=$controller->errors ?></div>
		
		<? endif; ?>
		
		<? if( $controller->success ): ?>
					
			<div class="alert alert-success" role="alert"><?=$controller->success ?></div>
		
		<? endif; ?>
					
		<div class="col-md-4">
		  <h2>Ice Cream Cone</h2>
		  <form action='/' method='post' class='form-horizontal'>
					
				<input type='hidden' name='product_type' value='cone'>
				
				<div class="form-group">
					<label for="scoop1-name" class="col-sm-3 control-label">Scoop 1</label>

					<div class="col-sm-6">
						<select name='scoops[]' class='form-control'>
							<? foreach( IceCream::getFlavors() as $flavor ): ?>
								<option value='<?=$flavor ?>'><?=$flavor ?></option>
							<? endforeach; ?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label for="scoop2-name" class="col-sm-3 control-label">Scoop 2 (optional)</label>

					<div class="col-sm-6">
						<select name='scoops[]' class='form-control'>
							<option value=''>( Choose Second Scoop )</option>
							<? foreach( IceCream::getFlavors() as $flavor ): ?>
								<option value='<?=$flavor ?>'><?=$flavor ?></option>
							<? endforeach; ?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-6">
						<button type="submit" name='submit' class="btn btn-default" value="Add">
							<i class="fa fa-btn fa-plus"></i>Add to Cart
						</button>
					</div>
				</div>
				
			</form>
		</div>
		<div class="col-md-4">
		  <h2>Milkshake</h2>
		  <form action='/' method='post' class='form-horizontal'>
					
				<input type='hidden' name='product_type' value='shake'>
				
				<div class="form-group">
					<label for="scoops-name" class="col-sm-3 control-label">Ice Cream</label>

					<div class="col-sm-6">
						<select name='scoops[]' class='form-control'>
							<? foreach( IceCream::getFlavors() as $flavor ): ?>
								<option value='<?=$flavor ?>'><?=$flavor ?></option>
							<? endforeach; ?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label for="milk-name" class="col-sm-3 control-label">Milk</label>

					<div class="col-sm-6">
						<select name='milk' class='form-control'>
							<? foreach( Milk::getTypes() as $type ): ?>
								<option value='<?=$type ?>'><?=$type ?></option>
							<? endforeach; ?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-6">
						<button type="submit" name='submit' class="btn btn-default" value="Add">
							<i class="fa fa-btn fa-plus"></i>Add to Cart
						</button>
					</div>
				</div>
				
			</form>

		</div>
		<div class="col-md-4">
		  <h2>Float</h2>
		  <form action='/' method='post' class='form-horizontal'>
					
				<input type='hidden' name='product_type' value='float'>
				
				<div class="form-group">
					<label for="soda-name" class="col-sm-3 control-label">Soda</label>

					<div class="col-sm-6">
						<select name='soda' class='form-control'>
							<? foreach( Soda::getTypes() as $type ): ?>
								<option value='<?=$type ?>'><?=$type ?></option>
							<? endforeach; ?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label for="scoops-name" class="col-sm-3 control-label">Scoop #1</label>

					<div class="col-sm-6">
						<select name='scoops[]' class='form-control'>
							<? foreach( IceCream::getFlavors() as $flavor ): ?>
								<option value='<?=$flavor ?>'><?=$flavor ?></option>
							<? endforeach; ?>
						</select>
						
					</div>
				</div>
				
				<div id='add-form-group' class="form-group">
					<label for="addscoop-name" class="col-sm-3 control-label">&nbsp;</label>

					<div class="col-sm-6">
						<a href='#' id='add-scoop' class='pull-right'><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add Scoop</a>
						
					</div>
				</div>
				
				<div id='new-scoop' class="form-group hidden" >
					<label for="scoops-name" class="col-sm-3 control-label"></label>

					<div class="col-sm-6">
						<select name='scoops[]' class='form-control'>
							<option value=''>Choose Flavor</option>
							<? foreach( IceCream::getFlavors() as $flavor ): ?>
								<option value='<?=$flavor ?>'><?=$flavor ?></option>
							<? endforeach; ?>
						</select>
						
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-6">
						<button type="submit" name='submit' class="btn btn-default" value="Add">
							<i class="fa fa-btn fa-plus"></i>Add to Cart
						</button>
					</div>
				</div>
				
			</form>
		</div>
	</div>
</div>

<? $items = Cart::getItems(); ?>

<div class="container">
	<div class="row">
		
		<h2>Shopping Cart</h2>
		
		<a class="btn btn-<?=(Discount::getDiscount() == 0)?'primary':'default' ?>" href="/?setDiscount=reset" role="button">No Discount</a>
		
		<? foreach( Discount::getAvailableDiscounts() as $discount ): ?>
		
			<a class="btn btn-<?=($discount == Discount::getDiscount())?'primary':'default' ?>" href="/?setDiscount=<?=$discount ?>" role="button"><?=$discount ?>%</a>
			
		<? endforeach; ?>
		<br>
		<br>
		
		<? if( count($items) ): ?>
		
			<?
				
				$totalDiscount 	= 0;
				$totalPrice		= 0;
				
			?>
		
			<table class="table"> 
				<thead> 
					<tr> 
						<th>Item</th> 
						<th>Quantity</th> 
						<th>Unit Price</th> 
						<th>Total Discount</th> 
						<th>Total Price</th> 
					</tr> 
				</thead> 
				<tbody> 
					<? foreach( $items as $index => $item ): ?>
					
					<?
				
						$totalDiscount 	+= $item['item']->getDiscount() ;
						$totalPrice		+= $item['item']->getSalePrice();
						
					?>
					<tr> 
						<td>
							<?=$item['item']->toString() ?><br>
							<a href='/?deleteItem=<?=$index ?>'>delete</a>
						</td> 
						<td><?=$item['quantity'] ?></td> 
						<td>$<?=number_format( $item['item']->getUnitPrice(), 2 ) ?></td> 
						<td>$<?=number_format( $item['item']->getDiscount(), 2 ) ?></td> 
						<td>$<?=number_format( $item['item']->getSalePrice(), 2 ) ?></td> 
					</tr> 
					<? endforeach; ?>
					
					<tr> 
						<td>Total</td> 
						<td>&nbsp;</td> 
						<td>&nbsp;</td> 
						<td>$<?=number_format( $totalDiscount, 2 ) ?></td> 
						<td>$<?=number_format( $totalPrice, 2 ) ?></td> 
					</tr> 
					
				</tbody> 
			</table>
			
		<? else: ?>
		
			<div class="alert alert-info" role="alert">There are no items in your cart.</div>
		
		<? endif; ?>
		
	</div>
</div>

<script>

$(document).ready(function(){
	
	$('#add-scoop').click(function(e){
		
		e.preventDefault();
		
		$('#new-scoop').clone().insertBefore('#add-form-group').removeClass('hidden').addClass('new-scoops').find('label').html('Scoop #' + ( $('.new-scoops').length + 1 ));
		
	});

});

</script>
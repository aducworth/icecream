<?
	class Float extends Item {
		
		public function __construct() {
			
			$this->name					= 'Float';
			$this->discount 			= true; 	
			$this->scoopsAllowed		= 0;		
			$this->flavorsAllowed		= 0;		
			$this->containsMilk			= false;
			$this->containsSoda			= true;
			$this->vessel				= 'Cup';
		
		}
		
		public function getUnitPrice() {
			
			// start with a base of soda + 1 scoop
			$price 					= 3;
			$additionalScoopPrice 	= 1;
			
			// add for each additional scoop
			if( count($this->scoops) > 1 ) {
				
				$price += ( ( count($this->scoops) - 1 ) * $additionalScoopPrice );
				
			}
			
			return $price;
			
		}
				
	}
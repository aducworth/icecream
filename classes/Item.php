<?
	
	abstract class Item {
				
		// individual product variables
		protected $name;
		protected $discount 			= false; 	// does this item allow discounts
		protected $scoopsAllowed		= 0;		// zero will equal infinity in our case
		protected $flavorsAllowed		= 0;		// zero will equal infinity in our case
		protected $containsMilk			= false;
		protected $containsSoda			= false;
		
		// ingredients
		protected $scoops 				= [];		// scoops of ice cream in this item
		protected $milk;							// milk used, if any
		protected $soda;							// milk used, if any
		protected $vessel;							// how the item will be served
		
		// errors
		protected $errors;
		
		// these will be implemented by each item since there isn't a standard pricing setup
		abstract public function getUnitPrice();
		
		public function getSalePrice() {
			
			return round( $this->getUnitPrice() - $this->getDiscount(), 2 );
			
		}
		
		public function getDiscount() {
			
			// make sure discounts are allowed
			if( $this->discount ) {
				
				return round( $this->getUnitPrice() * ( Discount::getDiscount() / 100 ), 2 );
				
			}
			
			return 0;
			
		}
		
		public function getErrors() {
			
			return $this->errors;
			
		}
				
		public function buildItem($iceCream, $milk = null, $soda = null) {
			
 			return ( $this->addIceCream($iceCream) && $this->addMilk($milk) && $this->addSoda($soda) );
			
		}
		
		public function toString() {
			
			$toreturn = $this->name . '<br>';
			
			// add scoops
			$toreturn .= ( 'Scoops: ' .  $this->consolidateScoopString() . '<br>' );
			
			if( $this->containsMilk ) {
				
				$toreturn .= 'Milk: ' . $this->milk . '<br>';
				
			}
			
			if( $this->containsSoda ) {
				
				$toreturn .= 'Soda: ' . $this->soda . '<br>';
				
			}
			
			$toreturn .= 'Serving Vessel: ' . $this->vessel;
			
			return $toreturn;
			
		}
		
		// instead of listing chocolate 3 times, list chocolate and the number 3
		public function consolidateScoopString() {
			
			$counts 	= array();
			$displays 	= array();
			
			foreach( $this->scoops as $flavor ) {
				
				$counts[$flavor]++;
				
			}
			
			foreach( $counts as $flavor => $count ) {
				
				$displays[] = $flavor . (($count>1)?("(" . $count . ")"):"");
				
			}
			
			return implode(',', $displays);
			
		}
		
		protected function addIceCream($iceCream) {
			
			// check to make sure they added ice cream
			if( $iceCream && is_array($iceCream) ) {
				
				// make sure no empty values are entered
				$iceCream = array_filter($iceCream);
				
				// check to make sure the number of scoops is valid
				if( count( $iceCream ) <= $this->scoopsAllowed || $this->scoopsAllowed == 0 ) {
					
					if( count(array_unique($iceCream)) <= $this->flavorsAllowed || $this->flavorsAllowed == 0 ) {
						
						foreach( $iceCream as $flavor ) {
							
							if( IceCream::isValid($flavor) ) {
								
								$this->scoops[] = $flavor;
								
							} else {
								
								$this->errors = $flavor . ' is not a valid flavor of ice cream.';
								
								return false;
								
							}
							
							
						}
						
					} else {
						
						$this->errors = 'You are only allowed ' . $this->flavorsAllowed . ' flavor(s) of ice cream.';
						
						return false;
					}
				
				} else {
					
					$this->errors = 'You are only allowed ' . $this->scoopsAllowed . ' flavor(s) of ice cream.';
					
					return false;
					
				}
				
			} else {
				
				$this->errors = 'You forgot the ice cream!';
				
				return false;
				
			}
			
			return true;
			
		}
		
		protected function addMilk($milk) {
			
			if($this->containsMilk) {
				
				if($milk) {
					
					if(Milk::isValid($milk)) {
						
						$this->milk = $milk;
						
					} else {
						
						$this->errors = $milk . ' is not a valid kind of milk!';
						
						return false;
						
					}
					
					
				} else {
					
					$this->errors = 'You forgot the milk!';
					
					return false;
					
				}
				
			}
			
			return true;
			
		}	
		
		protected function addSoda($soda) {
			
			if($this->containsSoda) {
				
				if($soda) {
					
					if(Soda::isValid($soda)) {
						
						$this->soda = $soda;
						
					} else {
						
						$this->errors = $soda . ' is not a valid kind of soda!';
						
						return false;
						
					}
					
					
				} else {
					
					$this->errors = 'You forgot the soda!';
					
					return false;
					
				}
				
			}
			
			return true;	
			
		}
		
	}
<?
	
	class Discount {
		
		protected static $available = array(
										10,
										20
		);
		
		public static function getAvailableDiscounts() {
			
			return static::$available;
			
		}
		
		public static function getDiscount() {
			
			if( isset($_SESSION['discount']) ) {
				
				return $_SESSION['discount'];
				
			}
			
			return 0;
			
		}
		
		public static function setDiscount($amount) {
			
			if( static::isValid($amount) ) {
				
				$_SESSION['discount'] = $amount;
				
				return true;
				
			}	
			
			return false;	
			
		}
		
		public static function clearDiscount() {
			
			$_SESSION['discount'] = 0;
			
		}
		
		public static function isValid($amount) {
			
			if(in_array($amount, static::$available)) {
				
				return true;
				
			}
			
			return false;
			
		}
		
	}
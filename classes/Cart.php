<?
	
	class Cart {
		
		public static function getItems() {
			
			if( $_SESSION['cart'] && is_array($_SESSION['cart']) ) {
				
				return $_SESSION['cart'];
				
			}
			
		}
		
		public static function addItem($item) {
			
			$added = false;
			
			$_SESSION['cart'] = is_array($_SESSION['cart'])?$_SESSION['cart']:array();
			
			foreach( $_SESSION['cart'] as &$cartItem ) {
				
				if( $cartItem['item'] == $item ) {
					
					$cartItem['quantity']++;
					$added = true;
					
				}
				
			}
			
			if( !$added ) {
				
				$_SESSION['cart'][] = array( 'quantity' => 1, 'item' => $item );
				
			}
			
			
		}
		
		public static function clearItems() {
			
			$_SESSION['cart'] = array();
			
		}
		
		public static function removeItem($index) {
			
			if(isset($_SESSION['cart'][$index])) {
				
				unset($_SESSION['cart'][$index]);
				
			}
		}
		
	}
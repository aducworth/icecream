<?
	class Milkshake extends Item {
		
		public function __construct() {
			
			$this->name					= 'Milkshake';
			$this->discount 			= true; 	
			$this->scoopsAllowed		= 0;		
			$this->flavorsAllowed		= 1;		
			$this->containsMilk			= true;
			$this->containsSoda			= false;
			$this->vessel				= 'Cup';
		
		}
		
		public function getUnitPrice($discount = null) {
			
			$price = 5;
			
			if( $discount ) {
				
				return ( $price * ( 1 - $discount ) );
				
			} 
			
			return $price;
			
		}
		
	}
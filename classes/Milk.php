<?
	
	class Milk {
		
		protected static $types = array(
										'Skim',
										'2%',
										'Whole'
		);
		
		public static function getTypes() {
			
			return static::$types;
			
		}
		
		public static function isValid($type) {
			
			if(in_array($type, static::$types)) {
				
				return true;
				
			}
			
			return false;
			
		}
		
	}
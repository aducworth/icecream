<?
	
	class IceCreamCone extends Item {
				
		public function __construct() {
			
			$this->name					= 'Ice Cream Cone';
			$this->discount 			= false; 	
			$this->scoopsAllowed		= 2;		
			$this->flavorsAllowed		= 2;		
			$this->containsMilk			= false;
			$this->containsSoda			= false;
			$this->vessel				= 'Waffle Cone';
		
		}
		
		public function getUnitPrice() {
			
			if( count($this->scoops) == 2 ) {
				
				return 5;
				
			} 
			
			return 3;
			
		}
		
	}
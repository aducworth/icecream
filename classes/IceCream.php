<?
	
	class IceCream {
		
		protected static $flavors = array(
										'Chocolate',
										'Vanilla',
										'Birthday Cake',
										'Coffee',
										'Salted Caramel',
										'Mint Chocolate Chip',
										'Cookies n Cream',
										'Chocolate Peanut Butter',
										'Butter Pecan',
										'Moose Tracks',
										'Pistachio'
		);
		
		public static function getFlavors() {
			
			return static::$flavors;
			
		}
		
		public static function isValid($flavor) {
			
			if(in_array($flavor, static::$flavors)) {
				
				return true;
				
			}
			
			return false;
			
		}
		
	}
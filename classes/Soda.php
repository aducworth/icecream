<?
	
	class Soda {
		
		protected static $types = array(
										'Coke',
										'Dr. Pepper',
										'Mountain Dew'
		);
		
		public static function getTypes() {
			
			return static::$types;
			
		}
		
		public static function isValid($type) {
			
			if(in_array($type, static::$types)) {
				
				return true;
				
			}
			
			return false;
			
		}
		
	}
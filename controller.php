<?
	
	class Controller {
		
		public $errors;
		
		public function index() {
			
			if( count($_POST) ) {
				
				// get started with the appropriate product
				if( $_POST['product_type'] == 'cone' ) {
					
					$item = new IceCreamCone();					
					
				} else if( $_POST['product_type'] == 'shake' ) {
					
					$item = new MilkShake();	
					
				} else if( $_POST['product_type'] == 'float' ) {
					
					$item = new Float();	
					
				} else {
					
					$this->errors = "Please select a valid product.";
					
				}
				
				// build the product from the options
				if( is_object($item) ) {
					
					// try to build the item
					if( $item->buildItem($_POST['scoops'],$_POST['milk'],$_POST['soda']) ) {
						
						Cart::addItem($item);
						
						$this->success = "Item added to cart!";
						
					} else {
						
						$this->errors = $item->getErrors();
						
					}
					
				}
				
			}
			
			// action for deleting item
			if(isset($_GET['deleteItem'])) {
				
				Cart::removeItem($_GET['deleteItem']);
				
			}
			
			// action for setting discount
			if(isset($_GET['setDiscount'])) {
				
				if( $_GET['setDiscount'] == 'reset' ) {
					
					Discount::clearDiscount();
					
					$this->success = "Discount has been removed.";
					
				} else {
					
					if( Discount::setDiscount($_GET['setDiscount']) ) {
						
						$this->success = "A " . $_GET['setDiscount'] . "% discount has been added!";
						
					} else {
						
						$this->errors = "There was a problem adding this discount.";
						
					}
					
					
					
				}
				
			}
			
		}
		
		// set up test data
		public function testData() {
			
			// test an ice cream cone
			$cone = new IceCreamCone();
		
			if( $cone->buildItem(array('Chocolate','Mint Chocolate Chip')) ) {
				
				Cart::addItem($cone);
				
			} else {
				
				echo( "Errors: " . $cone->getErrors() );
				
			}
			
			
			// test a milk shake
			$shake = new MilkShake();
			
			if( $shake->buildItem(array('Coffee'),'Skim') ) {
				
				Cart::addItem($shake);
				
			} else {
				
				echo( "Errors: " . $shake->getErrors() );
				
			}
			
			// test a float
			$float = new Float();
			
			if( $float->buildItem(array('Salted Caramel','Moose Tracks'),null,'Coke') ) {
				
				Cart::addItem($float);
				
			} else {
				
				echo( "Errors: " . $float->getErrors() );
				
			}
			
			header('Location: /?setDiscount=20');
			exit;
			
		}
		
		// set up invalid tests
		public function testFailures() {
			
			$eol = "<br>";
			
			// test an invalid item
			$invalidFlavor = new IceCreamCone();
		
			if( $invalidFlavor->buildItem(array('Invalid Chocolate','Mint Chocolate Chip')) ) {
				
				echo( "Invalid flavor added" . $eol );
				
			} else {
				
				echo( "Invalid Flavor: " . $invalidFlavor->getErrors() . $eol );
				
			}
			
			// test an invalid number of scoops for a cone
			$threeFlavorNumber = new IceCreamCone();
			
			if( $threeFlavorNumber->buildItem(array('Chocolate','Vanilla','Mint Chocolate Chip')) ) {
				
				echo( "Three favor cone added" . $eol );
				
			} else {
				
				echo( "Invalid Flavor Number: " . $threeFlavorNumber->getErrors() . $eol );
				
			}
			
			// test a milk shake with no milk
			$noMilk = new MilkShake();
			
			if( $noMilk->buildItem(array('Chocolate')) ) {
				
				echo( "Added a milk shake with no milk." . $eol );
				
			} else {
				
				echo( "No Milk: " . $noMilk->getErrors() . $eol );
				
			}
			
			// test a float with no soda
			$noSoda = new Float();
			
			if( $noSoda->buildItem(array('Chocolate','Vanilla'),null) ) {
				
				echo( "No soda float added." . $eol );
				
			} else {
				
				echo( "No Soda: " . $noSoda->getErrors() . $eol );
				
			}
			
			exit;
			
		}
						
	}